# setup.py
from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

setup(
    name='fibonacci',
    version='0.1.0',
    description='Sample solution for generating the fibonacci sequence for UrtheCast',
    long_description=readme,
    author='Matthew McCann',
    author_email='matmccann@gmail.com',
    url='https://gitlab.com/matmccann/fibonacci',
    packages=find_packages(exclude=('tests'))
)
