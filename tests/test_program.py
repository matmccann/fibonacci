# -*- coding: utf-8 -*-
import unittest
from .context import Solution1
from .context import requestUserInput, requestProgramContinues


class TestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_Solution1(self):
        print()
        fibonacci = Solution1()
        self.assertIsNotNone(fibonacci)
        test_values = [9, 10, 11, 100]
        test_answers = {9: 34, 10: 55, 11: 89, 100: 354224848179261915075}
        results = [fibonacci.calculate(x) for x in test_values]
        results = dict(zip(test_values, results))
        self.assertEqual(test_answers, fibonacci.computed_values)

    def test_Solution1_multiplyPowerMatrix(self):
        F = [[1, 1], [1, 0]]
        M = [[1, 1], [1, 0]]
        fibonacci = Solution1()
        fibonacci.multiplyPowerMatrix(F, M)
        T = [[2, 1], [1, 1]]
        self.assertEqual(T, F)

    def test_Solution1getFibPower(self):
        F = [[1, 1], [1, 0]]
        n = 9
        fibonacci = Solution1()
        self.assertEqual(fibonacci.computed_values, {})
        fibonacci.getFibPower(F, n - 1)
        self.assertEqual(F[0][0], 34)


if __name__ == '__main__':
    unittest.main()
