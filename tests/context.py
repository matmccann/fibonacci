# -*- coding: utf-8 -*-
import sys
import os
from fibonacci.fibonacciCalculator import Solution1
from fibonacci.user_interface import requestProgramContinues, requestUserInput
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
