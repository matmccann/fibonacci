# fibonacciCalculator.py
from time import time
import math
from functools import lru_cache

DEFAULT_FMT = '{function_name} ({elapsed: 0.12f}s): result is {result}'
SOL2_FMT = '{function_name} {_args[1]} ({elapsed: 0.12f}s): result is {result}'


def clock(fmt=DEFAULT_FMT):
    """ @clock() is a parametized registration decorator """
    def decorate(func):
        def clocked(*_args):
            t0 = time()
            _result = func(*_args)
            elapsed = time() - t0
            function_name = func.__name__
            args = ', '.join(repr(arg) for arg in _args)
            result = repr(_result)
            if fmt is not None:
                print(fmt.format(**locals()))
            return _result
        return clocked
    return decorate


class Solution1(object):
    """
    FibonacciCalculator is a singleton class used to calculate
    and store a record of the requested fibonacci sequence
    """
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self.computed_values = {}

    def __repr__(self):
        return str(self.computed_values)

    @clock()
    def calculate(self, n: int):
        """ returns n+1 sequence as power calculation result in F[0][0]"""
        F = [[1, 1], [1, 0]]
        if (n == 0):
            return 0
        self.getFibPower(F, n - 1)
        self.computed_values[n] = F[0][0]
        return self.computed_values[n]

    def getFibPower(self, F: list, n: int):
        """ calculate power(M,n) to get n+1 sequence """
        M = [[1, 1], [1, 0]]
        [self.multiplyPowerMatrix(F, M) for i in range(2, n + 1)]

    def multiplyPowerMatrix(self, F: list, M: list):
        """ Power Matrix from https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/ """
        x = (F[0][0] * M[0][0] + F[0][1] * M[1][0])
        y = (F[0][0] * M[0][1] + F[0][1] * M[1][1])
        z = (F[1][0] * M[0][0] + F[1][1] * M[1][0])
        w = (F[1][0] * M[0][1] + F[1][1] * M[1][1])

        F[0][0] = x
        F[0][1] = y
        F[1][0] = z
        F[1][1] = w

    @clock()
    def formula(self, n):
        """ Raw Calculation to get value of nth sequence  """
        phi = (1 + math.sqrt(5)) / 2
        return round(pow(phi, n) / math.sqrt(5))


class Solution1_1(object):

    def calculate(self, n):

        F = [[1, 1], [1, 0]]
        if (n == 0):
            return 0
        self.power(F, n - 1)
        return F[0][0]

    def multiply(self, F, M):
        x = (F[0][0] * M[0][0] + F[0][1] * M[1][0])
        y = (F[0][0] * M[0][1] + F[0][1] * M[1][1])
        z = (F[1][0] * M[0][0] + F[1][1] * M[1][0])
        w = (F[1][0] * M[0][1] + F[1][1] * M[1][1])

        F[0][0] = x
        F[0][1] = y
        F[1][0] = z
        F[1][1] = w

    # Optimized version of power() in method 4
    def power(self, F, n):

        if(n == 0 or n == 1):
            return
        M = [[1, 1], [1, 0]]

        self.power(F, n // 2)
        self.multiply(F, F)

        if (n % 2 != 0):
            self.multiply(F, M)


class Solution2(object):
    """
    Uses lru cache (memoization) to run a recursive solution
    """
    def call_calculate(self, n: int):
        """ function created for timing total recursion  """
        return self.calculateb(n)

    @lru_cache(maxsize=1000)
    def calculate(self, n: int):
        """ gets solution by recursion """
        if type(n) != int:
            raise TypeError('n must be a positive int')
        if n < 1:
            raise ValueError('n must be a positive int')

        if n == 1:
            return 1
        elif n == 2:
            return 1
        elif n > 2:
            return self.calculate(n - 1) + self.calculate(n - 2)


MAX = 1000

# # Create an array for memoization
f = [0] * MAX


class Solution3(object):
    """
    Optimization of Power Matrix solution with lru caching.
    Leverages the square matrix identify to compute by recursion with memoization.
    """
    # Returns n'th fibonacci number using table f[]
    @lru_cache(maxsize=1000)
    def calculate(self, n):
        global f

        # Base cases
        if (n == 0):
            return 0
        if (n == 1 or n == 2):
            f[n] = 1
            return (f[n])

        # If fib(n) is already computed
        if (f[n]):
            return f[n]

        if(n & 1):
            k = (n + 1) // 2
        else:
            k = n // 2

        if((n & 1)):
            f[n] = (self.calculate(k) * self.calculate(k) + self.calculate(k - 1) * self.calculate(k - 1))
        else:
            f[n] = (2 * self.calculate(k - 1) + self.calculate(k)) * self.calculate(k)

        return f[n]
