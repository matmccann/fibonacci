# fibonacci/user_interface.py


def requestProgramContinues() -> bool:
    """ Controls if the program continues running or is stopped by user input"""
    user_request = input("Enter N or n to stop; any other key continues: ")
    if(user_request.lower() == 'n'):
        return False
    else:
        return True


def requestUserInput(fibonacci) -> None:
    """ main loop that requests user input to create answer to fibonacci(n) """
    program_state = True
    print("Welcome to the Fibonacci Calculator.  We will calculate the sequence to the 'nth' degree.")
    while(program_state):
        print('*' * 10)
        try:
            n = input('Enter an integer n: ')
            result = fibonacci.calculate(int(n))
            program_state = requestProgramContinues()
        except ValueError as err:
            try:
                program_state = requestProgramContinues()
            except ValueError as err:
                program_state = False
