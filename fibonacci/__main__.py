# fibonacci/__main__.py
from time import time
from fibonacci.fibonacciCalculator import Solution1, Solution2, Solution1_1, Solution3
from fibonacci.user_interface import requestUserInput


def display_alrorithm(values_to_compute, function, description):
    for item in values_to_compute:
        try:
            t0 = time()
            value = function.calculate(item)
            t1 = time()
            print('{}({})   ({:.10f}s): result is {}'.format(description, item, t1 - t0, value))

        except RecursionError as err:
            print('\n\n')
            print(err)
            print(' oopsy, try a smaller number.')


if __name__ == "__main__":
    program = True
    powerMatrix = Solution1()
    recursive = Solution2()
    powerMatrix_optimized = Solution1_1()
    matrixIdentities = Solution3()

    print('*' * 10)
    requestUserInput(powerMatrix)
    print('*' * 10)
    print('Here are your answers (n: fib(n))')
    print(powerMatrix)
    print('*' * 10)
    display_alrorithm(powerMatrix.computed_values, recursive, 'recursive')
    display_alrorithm(powerMatrix.computed_values, powerMatrix, 'powerMatrix')
    display_alrorithm(powerMatrix.computed_values, powerMatrix_optimized, 'powerMatrix_opt')
    display_alrorithm(powerMatrix.computed_values, matrixIdentities, 'matrixIdentities')

    print('Good-bye')
