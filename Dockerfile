# base image
FROM ubuntu:18.04

# install dependencies
RUN apt-get update && \
    apt -y upgrade && \
    apt install -y python3-pip python3-venv sudo build-essential libssl-dev libffi-dev python3-dev

# set working directory inside the docker container
RUN mkdir -p /code
WORKDIR /code

# add and install requirements
COPY ./requirements.txt /code/requirements.txt

# install requirements inside docker container
RUN pip3 install -r requirements.txt

# add app
COPY . /code

## keep the container running after it is built
#CMD tail -f /dev/null
