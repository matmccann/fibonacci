Pew pew pew
====================

|python|

Description
-----------

**Challenge Description**

- Write a function that calculates the nth Fibonacci number in O(n) time or better without using any "for" or "while" loops.

**Approach**

SOLUTION #1 (just make it work!)

- power matrix solution copy/pasted, setup a project, wrote some tests ...
- I used a singleton class that creates a list of all your function calls

- or just calculate it : Fn = {[(√5 + 1)/2] ^ n} / √5

.. code-block:: console

    $ python -m fibonacci
    >>> enter a number
    >>> press 'n' so you quit right away

SOLUTION #2 (Recursion with memoization)

- recursive solution that stores its values for quick lookup
- uses LRU memoization

SOLUTION #3 (Optimized recursion with memoization)

- Leverages the square matrix identify to compute by recursion with memoization

**Future Improvements**

- investigate memory methods with numbers.



Quickstart
----------
- run this from the project root directory

.. code-block:: console

    $ python -m venv venv
    $ pip install -e .
    $ python -m fibonacci


Testing
-------
- run this from the project root directory

.. code-block:: console

    $ python -m unittest -vvv


.. |forthebadge made-with-python| image:: http://ForTheBadge.com/images/badges/made-with-python.svg
   :target: https://www.python.org/

.. |Build Status| image:: https://gitlab.com/matmccann/fibonacci/badges/master/build.svg
   :target: https://gitlab.com/matmccann/fibonacci/commits/master

.. |python| image:: https://img.shields.io/pypi/pyversions/hypercorn.svg
   :target: https://pypi.python.org/pypi/Hypercorn/
